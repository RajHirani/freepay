package com.freepay.ui.Product;

import java.net.URL;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

public class AddProductController implements Initializable{
	
	private static final Logger log = LoggerFactory.getLogger(AddProductController.class);

	@FXML
	private JFXTextField productId;

	@FXML
	private JFXTextField productName;

	@FXML
	private JFXTextField productPrice;

	@FXML
	private JFXButton saveButton;

	@FXML
	private JFXButton cancelButton;

	@FXML
	void onCanelClick(ActionEvent event) {
		log.debug("On Cancel Button Called");
	}

	@FXML
	void onSaveClick(ActionEvent event) {
		log.debug("On Save Button Called");
	}

	public void initialize(URL location, ResourceBundle resources) {
		
	}

}
